const { app, BrowserWindow } = require('electron');
const isDev = require('electron-is-dev');
const path = require('path');
require('electron-reload');

let mainWindow;

const createWindow = () => {
	mainWindow = new BrowserWindow({
		minWidth: 900,
		minHeight: 900,
		show: false,
		UseContentSize: true,
	});
	const startURL = isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`;

	mainWindow.loadURL(startURL);
	mainWindow.removeMenu();

	mainWindow.once('ready-to-show', () => mainWindow.show());
	mainWindow.on('closed', () => {
		mainWindow = null;
	});
};
app.on('ready', createWindow);
