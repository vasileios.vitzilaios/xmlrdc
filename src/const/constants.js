export const xmlType = {
	CSRD2MOCK: {
		id: 1,
		name: "CSRD2 Mock",
	},
	CSRD2PROD: {
		id: 2,
		name: "CSRD2 Prod",
	},
	CSRD2ALL: {
		id: 3,
		name: "All",
	},
};

export const xmlTypeOutput = {
	ENTITY: {
		id: 1,
		name: "CSRD2 Entity",
		value: "entity",
	},
	ENTRY: {
		id: 2,
		name: "CSRD2 Entry",
		value: "entries",
	},
	BOTH: {
		id: 3,
		name: "CSRD2 Both",
		value: "both",
	},
};
