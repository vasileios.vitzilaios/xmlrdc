import React from "react";
import MuiAlert from "@material-ui/lab/Alert";
import {Snackbar} from "@material-ui/core";

function Alert(props) {
	return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const CustomSnackBar = ({status, open, handleClose}) => {

	const snackSuccessMessage = () => (
		<span>
			<b>SUCCESS:</b> Xml successfully created!
		</span>
	);

	const snackErrorMessage = () => (
		<span>
			<b>ERROR:</b> Things went South...
		</span>
	);


	return (
		<Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
			<Alert onClose={handleClose} severity={status ? "success" : "error"}>
				{status ? snackSuccessMessage() : snackErrorMessage()}
			</Alert>
		</Snackbar>
	);
};

export default CustomSnackBar;
