import GridItem from "../../material/Grid/GridItem";
import CustomInput from "../../material/CustomInput/CustomInput";
import React from "react";

const CustomValuesComponent = (props) => {
	const { codeDescValues, handleChange } = props;

	return (
		<GridItem
			container
			style={{ padding: 0 }}
			direction="column"
			justify="center"
			alignItems="center"
		>
			<GridItem xs={12} sm={12} md={4}>
				<CustomInput
					value={codeDescValues.code}
					labelText="Code"
					id="code"
					inputProps={{
						name: "codeSize",
						type: "number",
						min: "0",
						value: codeDescValues.codeSize,
						onChange: handleChange,
						required: true,
					}}
				/>
			</GridItem>
			<GridItem xs={12} sm={12} md={4}>
				<CustomInput
					labelText="Description"
					id="description"
					inputProps={{
						name: "descriptionSize",
						type: "number",
						min: "0",
						value: codeDescValues.descriptionSize,
						onChange: handleChange,
						required: true,
					}}
				/>
			</GridItem>
		</GridItem>
	);
};

export default CustomValuesComponent;
