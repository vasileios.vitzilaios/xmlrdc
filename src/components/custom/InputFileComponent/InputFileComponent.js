import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import GridContainer from "../../material/Grid/GridContainer";
import GridItem from "../../material/Grid/GridItem";
import Button from "../../material/CustomButtons/Button";
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
	margin: {
		margin: theme.spacing(1),
	},
	extendedIcon: {
		marginRight: theme.spacing(1),
	},
}));

const InputFileComponent = ({filename}) => {

	const classes = useStyles();

	return (
		<GridContainer direction='row'>
			<GridItem>
				{filename}
			</GridItem>
			<GridItem>
				<Button size="small" className={classes.margin} startIcon={<DeleteIcon/>}/>
			</GridItem>
		</GridContainer>
	)
}

export default InputFileComponent;