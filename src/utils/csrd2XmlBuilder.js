import { xmlType, xmlTypeOutput } from "../const/constants";
import builder from "xmlbuilder";
import { saveAs } from "file-saver";
import {
	getEntityObjForMock,
	getEntityObjForProd,
	getEntriesObjForMock,
	getEntriesObjForProd,
} from "./objectProvider";

export const beginParse = (file) => {
	return new Promise((resolve, reject) => {
		const fr = new FileReader();
		
		fr.onload = (e) => {
			resolve({
				file: file.item(0),
				items: fr.result,
			});
		};
		fr.onerror = reject;
		
		fr.readAsText(file.item(0));
	});
};

export const convertToXmlForCSRD2 = (
	selectType,
	radioValue,
	data,
	codeDescValues
) => {
	const fileData = [];
	
	if (selectType === xmlType.CSRD2MOCK.id) {
		switch (radioValue) {
		case xmlTypeOutput.ENTITY.value:
			fileData.push(convertToEntityXmlForMock(data, codeDescValues));
			break;
		case xmlTypeOutput.ENTRY.value:
			fileData.push(convertToEntryXmlForMock(data));
			break;
		default:
			fileData.push(convertToEntityXmlForMock(data, codeDescValues));
			fileData.push(convertToEntryXmlForMock(data));
		}
	} else if (selectType === xmlType.CSRD2PROD.id) {
		switch (radioValue) {
		case xmlTypeOutput.ENTITY.value:
			fileData.push(convertToEntityXmlForProd(data, codeDescValues));
			break;
		case xmlTypeOutput.ENTRY.value:
			fileData.push(convertToEntryXmlForProd(data));
			break;
		default:
			fileData.push(convertToEntityXmlForProd(data, codeDescValues));
			fileData.push(convertToEntryXmlForProd(data));
		}
	} else {
		switch (radioValue) {
		case xmlTypeOutput.ENTITY.value:
			fileData.push(convertToEntityXmlForMock(data, codeDescValues));
			fileData.push(convertToEntityXmlForProd(data, codeDescValues));
			break;
		case xmlTypeOutput.ENTRY.value:
			fileData.push(convertToEntryXmlForMock(data));
			fileData.push(convertToEntryXmlForProd(data));
			break;
		default:
			fileData.push(convertToEntityXmlForMock(data, codeDescValues));
			fileData.push(convertToEntryXmlForMock(data));
			fileData.push(convertToEntityXmlForProd(data, codeDescValues));
			fileData.push(convertToEntryXmlForProd(data));
		}
	}
	
	return Promise.resolve(fileData);
};

const convertToEntityXmlForMock = (data, codeDescValues) => {
	const names = constructNamesForEntities(data);
	
	const anSize = calcANSize(getJsonItems(data), codeDescValues);
	
	const obj = getEntityObjForMock(names, anSize);
	
	const xml = builder
		.create(obj, {
			version: "1.0",
			encoding: "utf-8",
			standalone: true,
		})
		.end({ pretty: true });
	return {
		data: xml,
		filename: names.entityFileNameMock,
		type: "application/xml",
	};
};

const convertToEntryXmlForMock = (data) => {
	const names = constructNamesForEntries(data);
	
	const items = getJsonItems(data);
	
	const obj = getEntriesObjForMock(names, items);
	
	const xml = builder
		.create(obj, {
			version: "1.0",
			encoding: "utf-8",
			standalone: true,
		})
		.end({ pretty: true });
	return {
		data: xml,
		filename: names.entryFileNameMock,
		type: "application/xml",
	};
};

const convertToEntityXmlForProd = (data, codeDescValues) => {
	const names = constructNamesForEntities(data);
	
	const anSize = calcANSize(getJsonItems(data), codeDescValues);
	
	const obj = getEntityObjForProd(names, anSize);
	
	const xml = builder
		.create(obj, {
			version: "1.0",
			encoding: "utf-8",
			standalone: true,
		})
		.end({ pretty: true });
	return {
		data: xml,
		filename: names.entityFileNameProd,
		type: "application/xml",
	};
};

const convertToEntryXmlForProd = (data) => {
	const names = constructNamesForEntries(data);
	
	const items = getJsonItems(data);
	
	const obj = getEntriesObjForProd(names, items);
	
	const xml = builder
		.create(obj, {
			version: "1.0",
			encoding: "utf-8",
			standalone: true,
		})
		.end({ pretty: true });
	return {
		data: xml,
		filename: names.entryFileNameProd,
		type: "application/xml",
	};
};

export const exportToXml = (fileData) => {
	for (const file of fileData) {
		const {
			data,
			filename,
			type
		} = file;
		const blob = new Blob([data], { type: type.concat(";charset=utf-8") });
		saveAs(blob, filename);
	}
	
	return Promise.resolve(true);
};

export const exportToXml2 = (fileData) => {
	const file = new Blob([fileData.data], { type: fileData.type });
	if (window.navigator.msSaveOrOpenBlob) {
		// IE10+
		window.navigator.msSaveOrOpenBlob(file, fileData.filename);
	} else {
		// Others
		const a = document.createElement("a"),
			url = URL.createObjectURL(file);
		a.href = url;
		a.download = fileData.filename;
		document.body.appendChild(a);
		a.click();
		setTimeout(function () {
			document.body.removeChild(a);
			window.URL.revokeObjectURL(url);
		}, 0);
	}
};

const calcANSize = (items, codeDescValues) => {
	let codeSize = 0;
	let descSize;
	
	console.log(codeDescValues.codeSize);
	console.log(codeDescValues.descriptionSize);
	
	if (codeDescValues.codeSize !== 0) {
		codeSize = codeDescValues.codeSize;
	} else {
		items.forEach((item) => {
			if (item.code.length > codeSize) {
				codeSize = item.code.length;
			}
		});
	}
	
	if (
		codeDescValues.descriptionSize !== 0
	) {
		descSize = codeDescValues.descriptionSize;
	} else {
		descSize = 140;
	}
	
	return {
		codeSize,
		descSize,
	};
};

const getJsonItems = (data) => {
	try {
		const dataItems = JSON.parse(data.items);
		
		if (dataItems.results) {
			const { items } = dataItems.results[0];
			return items;
		} else {
			const { items } = dataItems;
			return items;
		}
	} catch (e) {
		return {};
	}
};

const constructNamesForEntities = (data) => {
	const noExtensionName = data.file.name.replace(/\.[^/.]+$/, "");
	const entityName = noExtensionName.substr(
		0,
		noExtensionName.lastIndexOf("_")
	);
	const fileNumber = noExtensionName.split("_")
		.pop();
	const entityFileNameMock = entityName
		.concat("-CL")
		.concat(fileNumber)
		.concat("-entities-Generic");
	const entityFileNameProd = "CL_Definition_CL"
		.concat(fileNumber)
		.concat("_")
		.concat(entityName);
	
	return {
		noExtensionName,
		entityName,
		fileNumber,
		entityFileNameMock,
		entityFileNameProd,
	};
};

const constructNamesForEntries = (data) => {
	const noExtensionName = data.file.name.replace(/\.[^/.]+$/, "");
	const entryName = noExtensionName.substr(0, noExtensionName.lastIndexOf("_"));
	const fileNumber = noExtensionName.split("_")
		.pop();
	const entryFileNameMock = entryName
		.concat("-CL")
		.concat(fileNumber)
		.concat("-all-Generic");
	const entryFileNameProd = "CL_Entries_CL"
		.concat(fileNumber)
		.concat("_")
		.concat(entryName);
	
	return {
		noExtensionName,
		entryName,
		fileNumber,
		entryFileNameMock,
		entryFileNameProd,
	};
};
