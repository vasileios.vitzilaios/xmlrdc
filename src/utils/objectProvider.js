import { v4 as uuidv4 } from "uuid";

export const getEntityObjForMock = (names, anSize) => {
	return {
		"ns31:RDEntityDefinitionsType": {
			"@xsi:schemaLocation": "",
			"@xmlns": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDStatusType/V3",
			"@xmlns:ns2": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityDefinitionType/V3",
			"@xmlns:ns4": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityFilterPairType/V2",
			"@xmlns:ns3": "http://xmlns.ec.eu/BusinessObjects/CSRD2/StatementsType/V2",
			"@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"@xmlns:ns6": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityLogicalDefinitionType/V2",
			"@xmlns:ns31": "http://xmlns.ec.eu/BusinessObjects/RDEntityDefinitionsType/V2",
			"@xmlns:ns5": "http://xmlns.ec.eu/BusinessObjects/CSRD2/FilterRefType/V2",
			"@xmlns:ns8": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDBaseRestrictionType/V3",
			"@xmlns:ns7": "http://xmlns.ec.eu/BusinessObjects/CSRD2/FilterType/V3",
			"@xmlns:ns13": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityFilterDefsType/V3",
			"@xmlns:ns9": "http://xmlns.ec.eu/BusinessObjects/CSRD2/FilterLsdListType/V3",
			"@xmlns:ns12": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDColumnRestrictionType/V3",
			"@xmlns:ns11": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRowRestrictionListType/V3",
			"@xmlns:ns10": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRestrictionType/V3",
			"@xmlns:ns17": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityRuleListType/V3",
			"@xmlns:ns16": "http://xmlns.ec.eu/BusinessObjects/CSRD2/LogicalRestrictionType/V3",
			"@xmlns:ns15": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRuleRestrictionListType/V3",
			"@xmlns:ns14": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRuleType/V3",
			"@xmlns:ns19": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ConstraintParameterType/V2",
			"@xmlns:ns18": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ComplexType/V2",
			"@xmlns:ns20": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ConstraintType/V2",
			"@xmlns:ns24": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ComplexTypeDefinitionsType/V2",
			"@xmlns:ns23": "http://xmlns.ec.eu/BusinessObjects/CSRD2/DataGroupType/V2",
			"@xmlns:ns22": "http://xmlns.ec.eu/BusinessObjects/CSRD2/DataItemType/V2",
			"@xmlns:ns21": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ConstraintsType/V2",
			"@xmlns:ns28": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RestrictionType/V2",
			"@xmlns:ns27": "http://xmlns.ec.eu/BusinessObjects/CSRD2/SimpleType/V3",
			"@xmlns:ns26": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityDefinitionsType/V3",
			"@xmlns:ns25": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityLsdType/V2",
			"@xmlns:ns29": "http://xmlns.ec.eu/BusinessObjects/CSRD2/SimpleTypeDefinitionsType/V3",
			"ns26:RDEntityDefinition": {
				"@name": names.entityName,
				"@number": names.fileNumber,
				"@type": names.entityName.concat("_0"),
				"ns2:RDEntityStatus": {
					"state": {
						"#text": "valid"
					},
					"activeFrom": {
						"#text": "1900-01-01"
					}
				},
				"ns2:RDEntityStatements": {
					"ns3:Statement": [
						{
							"@name": "RDEntity.DataProvider",
							"#text": "CRMS2"
						},
						{
							"@name": "RDEntity.CodeListType",
							"#text": "Technical"
						},
						{
							"@name": "RDEntity.EntityType",
							"#text": "CodeList"
						},
						{
							"@name": "RDEntity.Label",
							"#text": names.entityName
						},
						{
							"@name": "RDEntity.Description",
							"@lang": "en",
							"#text": names.entityName.concat(" description. The values of this codelist are also available and updated in CS/RD2.")
						},
						{
							"@name": "RDEntity.StructureType",
							"#text": "Complex with simple key"
						}
					]
				},
				"ns2:ComplexTypeDefinitions": {
					"ns24:ComplexType": {
						"@name": names.entityName.concat("_0"),
						"ns18:DataItem": {
							"@isprimarykey": "true",
							"@name": "Code",
							"@format": "an..".concat(anSize.codeSize),
							"@required": "true",
							"ns22:statement": {
								"@name": "RDSchemaElement.Label",
								"#text": names.entityName.concat(" Code")
							}
						}
					}
				},
				"ns2:RDEntityLsdType": {
					"ns25:lsdtype": {
						"#text": "an..".concat(anSize.descSize)
					}
				}
			},
			"ns26:SimpleTypeDefinitions": {}
		}
	};
};

export const getEntriesObjForMock = (names, items) => {
	return {
		"ns8:RDEntityEntryListType": {
			"@xsi:schemaLocation": "",
			"@xmlns": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDStatusType/V3",
			"@xmlns:ns6": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityEntryListType/V3",
			"@xmlns:ns5": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityType/V3",
			"@xmlns:ns8": "http://xmlns.ec.eu/BusinessObjects/RDEntityEntryListType/V2",
			"@xmlns:ns2": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntryType/V3",
			"@xmlns:ns4": "http://xmlns.ec.eu/BusinessObjects/CSRD2/LsdListType/V2",
			"@xmlns:ns3": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDValidityPeriodType/V2",
			"@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"ns6:RDEntity": {
				"@name": names.entryName,
				"#text": loopMockEntries(items)
			}
		}
	};
};

export const getEntityObjForProd = (names, anSize) => {
	return {
		"ns6:CreateReferenceDataEntityRequestType": {
			"@xmlns:ns1": "http://xmlns.ec.eu/BusinessObjects/CSRD2/MessageHeaderType/V2",
			"@xmlns:ns2": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityDefinitionsType/V3",
			"@xmlns:ns3": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityDefinitionType/V3",
			"@xmlns:ns4": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDStatusType/V3",
			"@xmlns:ns5": "http://xmlns.ec.eu/BusinessObjects/CSRD2/StatementsType/V2",
			"@xmlns:ns6": "http://ws.bas.entity.csrd.customs.taxud.ec.europa.eu",
			"@xmlns:ns7": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityLogicalDefinitionType/V2",
			"@xmlns:ns8": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityFilterPairType/V2",
			"@xmlns:ns10": "http://xmlns.ec.eu/BusinessObjects/CSRD2/FilterRefType/V2",
			"@xmlns:ns12": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityFilterDefsType/V3",
			"@xmlns:ns13": "http://xmlns.ec.eu/BusinessObjects/CSRD2/FilterType/V3",
			"@xmlns:ns14": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRowRestrictionListType/V3",
			"@xmlns:ns15": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRestrictionType/V3",
			"@xmlns:ns16": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDBaseRestrictionType/V3",
			"@xmlns:ns17": "http://xmlns.ec.eu/BusinessObjects/CSRD2/FilterLsdListType/V2",
			"@xmlns:ns19": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDColumnRestrictionType/V3",
			"@xmlns:ns20": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityRuleListType/V3",
			"@xmlns:ns21": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRuleType/V3",
			"@xmlns:ns22": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDRuleRestrictionListType/V3",
			"@xmlns:ns23": "http://xmlns.ec.eu/BusinessObjects/CSRD2/LogicalRestrictionType/V3",
			"@xmlns:ns24": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ComplexTypeDefinitionsType/V2",
			"@xmlns:ns25": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ComplexType/V2",
			"@xmlns:ns26": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ConstraintsType/V2",
			"@xmlns:ns27": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ConstraintType/V2",
			"@xmlns:ns28": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ConstraintParameterType/V2",
			"@xmlns:ns29": "http://xmlns.ec.eu/BusinessObjects/CSRD2/DataItemType/V2",
			"@xmlns:ns30": "http://xmlns.ec.eu/BusinessObjects/CSRD2/DataGroupType/V2",
			"@xmlns:ns31": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityLsdType/V2",
			"@xmlns:ns32": "http://xmlns.ec.eu/BusinessObjects/CSRD2/SimpleTypeDefinitionsType/V3",
			"@xmlns:ns33": "http://xmlns.ec.eu/BusinessObjects/CSRD2/SimpleType/V3",
			"@xmlns:ns34": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RestrictionType/V2",
			"@xmlns:ns0": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ReferenceDataEntityManagementBASServiceType/V3",
			"ns0:MessageHeader": {
				"ns1:AddressingInformation": {
					"ns1:messageID": "47b69ffc-fe99-495c-8cb1-ba4e47133def"
				},
				"ns1:attribute": {
					"@name": "Event.MessageChannel",
					"#text": "WEB"
				}
			},
			"ns0:RDEntityDefinitions": {
				"ns2:RDEntityDefinition": {
					"@name": names.entityName,
					"@number": names.fileNumber,
					"@type": names.entityName.concat("_0"),
					"ns3:RDEntityStatus": {
						"ns4:state": {
							"#text": "valid"
						},
						"ns4:activeFrom": {
							"#text": "1900-01-01"
						}
					},
					"ns3:RDEntityStatements": {
						"ns5:Statement": [
							{
								"@name": "RDEntity.DataProvider",
								"#text": "CRMS2"
							},
							{
								"@name": "RDEntity.CodeListType",
								"#text": "Business"
							},
							{
								"@name": "RDEntity.EntityType",
								"#text": "CodeList"
							},
							{
								"@name": "RDEntity.Label",
								"#text": names.entityName
							},
							{
								"@name": "RDEntity.Description",
								"@lang": "en",
								"#text": names.entityName.concat(" description. The values of this codelist are also available and updated in CS/RD2.")
							},
							{
								"@name": "RDEntity.StructureType",
								"#text": "Simple"
							}
						]
					},
					"ns3:ComplexTypeDefinitions": {
						"ns24:ComplexType": {
							"@name": names.entityName.concat("_0"),
							"ns25:DataItem": {
								"@isprimarykey": "true",
								"@name": "Code",
								"@format": "an..".concat(anSize.codeSize),
								"@required": "true",
								"ns29:statement": {
									"@name": "RDSchemaElement.Label",
									"#text": "Code"
								}
							}
						}
					},
					"ns3:RDEntityLsdType": {
						"ns31:lsdtype": {
							"#text": "an..".concat(anSize.descSize)
						}
					}
				},
				"ns2:SimpleTypeDefinitions": {}
			}
		}
	};
};

export const getEntriesObjForProd = (names, items) => {
	return {
		"ns7:CreateReferenceDataEntryRequestType": {
			"@xmlns:ns1": "http://xmlns.ec.eu/BusinessObjects/CSRD2/MessageHeaderType/V2",
			"@xmlns:ns2": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntryType/V3",
			"@xmlns:ns3": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDStatusType/V3",
			"@xmlns:ns4": "http://xmlns.ec.eu/BusinessObjects/CSRD2/LsdListType/V2",
			"@xmlns:ns5": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityType/V3",
			"@xmlns:ns6": "http://xmlns.ec.eu/BusinessObjects/CSRD2/RDEntityEntryListType/V3",
			"@xmlns:ns7": "http://ws.bas.entity.csrd.customs.taxud.ec.europa.eu",
			"@xmlns:ns0": "http://xmlns.ec.eu/BusinessObjects/CSRD2/ReferenceDataEntryManagementBASServiceType/V3",
			"ns0:MessageHeader": {
				"ns1:AddressingInformation": {
					"ns1:messageID": uuidv4(),
					"ns1:attribute": {
						"@name": "Event.MessageChannel",
						"#text": "WEB"
					}
				}
			},
			"ns0:RDEntityList": {
				"ns6:RDEntity": {
					"@name": names.entryName,
					"#text": loopProdEntries(items)
				}
			}
		}
	};
};

const loopMockEntries = (items) => {
	return items.map(item => {
		return {
			"ns5:RDEntry": {
				"ns2:RDEntryStatus": {
					"state": "valid",
					"activeFrom": "2000-01-01"
				},
				"ns2:dataItem": {
					"@name": "Code",
					"#text": item.code
				},
				"ns2:LsdList": {
					"ns4:description": {
						"@lang": "en",
						"#text": item.description
					}
				}
			}
		};
	});
};

const loopProdEntries = (items) => {
	return items.map(item => {
		return {
			"ns5:RDEntry": {
				"ns2:RDEntryStatus": {
					"ns3:state": "valid",
					"ns3:activeFrom": "2000-01-01"
				},
				"ns2:dataItem": {
					"@name": "Code",
					"#text": item.code
				},
				"ns2:LsdList": {
					"ns4:description": {
						"@lang": "en",
						"#text": item.description
					}
				}
			}
		};
	});
};
