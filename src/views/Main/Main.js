import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import {
	FormControlLabel,
	FormLabel,
	InputLabel,
	MenuItem,
	Popover,
	Radio,
	RadioGroup,
	Select,
	Switch,
} from "@material-ui/core";
import { AttachFile } from "@material-ui/icons";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import Grid from "@material-ui/core/Grid";
import { xmlType } from "../../const/constants";
import CustomFileInput from "../../components/material/CustomFileInput/CustomFileInput";
import Button from "../../components/material/CustomButtons/Button";
import {
	beginParse,
	convertToXmlForCSRD2,
	exportToXml,
} from "../../utils/csrd2XmlBuilder";
import CustomSnackBar from "../../components/custom/CustomSnackBar/CustomSnackBar";
import styles from "../../assets/jss/material-kit-pro-react/customCheckboxRadioSwitchStyle";
import popoverStyles from "../../assets/jss/material-kit-pro-react/popoverStyles";
import selectStyles from "../../assets/jss/material-kit-pro-react/customSelectStyle";
import CustomValuesComponent
	from "../../components/custom/CustomValuesComponent/CustomValuesComponent";

const usePopOverStyles = makeStyles(popoverStyles);
const useSelectStyles = makeStyles(selectStyles);
const useStyles = makeStyles(styles);

const Main = () => {
	const checkBoxStyles = useStyles();
	const popOverStyles = usePopOverStyles();
	const selectStyles = useSelectStyles();

	const [selectType, setSelectType] = useState(1);
	const [files, setFiles] = useState(null);
	const [open, setOpen] = useState(false);
	const [status, setStatus] = useState(null);
	const [checked, setChecked] = useState(false);
	const [anchorElRight, setAnchorElRight] = useState(null);
	const [radioValue, setRadioValue] = useState("both");
	const [codeDescValues, setCodeDescValues] = useState({
		codeSize: 0,
		descriptionSize: 0,
	});

	const handleSelect = (setFunc) => (e) => {
		setFunc(e.target.value);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (files && files.length > 0) {
			beginParse(files)
				.then((data) =>
					convertToXmlForCSRD2(
						selectType,
						radioValue,
						data,
						codeDescValues
					)
						.then((fileData) => exportToXml(fileData))
				)
				.then((status) => {
					setStatus(status);
					setOpen(true);
				});
		}
	};

	const handleSnackClose = (event, reason) => {
		if (reason === "clickaway") {
			return;
		}

		setOpen(false);
	};

	const handleChange = (e) => {
		const {
			name,
			value,
			type
		} = e.target;

		if (type === "number") {
			setCodeDescValues((prevState) => ({
				...prevState,
				[name]: parseInt(value),
			}));
		} else if (type === "radio") {
			setRadioValue(value);
		}
	};

	return (
		<form onSubmit={handleSubmit}>
			<Grid
				container
				direction="column"
				spacing={4}
				wrap="nowrap"
				justify="center"
				style={{
					paddingLeft: 225,
					paddingRight: 225,
				}}
			>
				<Grid item>
					<h1>XML Converter for Ref Data</h1>
				</Grid>
				<Grid item>
					<FormControl fullWidth className={selectStyles.selectFormControl}>
						<InputLabel
							htmlFor="simple-select"
							className={selectStyles.selectLabel}
						>
							XML Type
						</InputLabel>
						<Select
							MenuProps={{ className: selectStyles.selectMenu }}
							classes={{ select: selectStyles.select }}
							value={selectType}
							onChange={handleSelect(setSelectType)}
							inputProps={{
								name: "simpleSelect",
								id: "simple-select",
							}}
						>
							<MenuItem
								classes={{
									root: selectStyles.selectMenuItem,
									selected: selectStyles.selectMenuItemSelected,
								}}
								value={xmlType.CSRD2MOCK.id}
							>
								{xmlType.CSRD2MOCK.name}
							</MenuItem>
							<MenuItem
								classes={{
									root: selectStyles.selectMenuItem,
									selected: selectStyles.selectMenuItemSelected,
								}}
								value={xmlType.CSRD2PROD.id}
							>
								{xmlType.CSRD2PROD.name}
							</MenuItem>
							<MenuItem
								classes={{
									root: selectStyles.selectMenuItem,
									selected: selectStyles.selectMenuItemSelected,
								}}
								value={xmlType.CSRD2ALL.id}
							>
								{xmlType.CSRD2ALL.name}
							</MenuItem>
						</Select>
					</FormControl>
				</Grid>
				<Grid item>
					<FormControl component="fieldset">
						<FormLabel component="legend">Export</FormLabel>
						<RadioGroup
							aria-label="xmlExport"
							name="xmlExport"
							value={radioValue}
							onChange={handleChange}
						>
							<FormControlLabel
								value="entity"
								control={<Radio/>}
								label="Entity XML"
							/>
							<FormControlLabel
								value="entries"
								control={<Radio/>}
								label="Entries XML"
							/>
							<FormControlLabel value="both" control={<Radio/>} label="Both"/>
						</RadioGroup>
					</FormControl>
				</Grid>
				<Grid container direction="column" justify="center">
					<Grid item>
						<CustomFileInput
							formControlProps={{
								fullWidth: true,
							}}
							inputProps={{
								placeholder: "Single File...",
								accept: ".json",
							}}
							endButton={{
								buttonProps: {
									round: true,
									color: "primary",
									justIcon: true,
									fileButton: true,
								},
								icon: <AttachFile/>,
							}}
							files={files}
							setFiles={setFiles}
						/>
					</Grid>
					<Grid
						item
						style={{
							marginBottom: 40,
							marginTop: 40,
						}}
					>
						<Button
							color="primary"
							size="lg"
							round
							type="submit"
							disabled={files === null || files.length === 0}
						>
							Submit
						</Button>
					</Grid>
					<Grid item>
						<FormControlLabel
							control={
								<Switch
									checked={checked}
									onChange={(event) => {
										setChecked(event.target.checked);
										setCodeDescValues({
											codeSize: 0,
											descriptionSize: 0,
										});
									}}
									value="checkedA"
									classes={{
										switchBase: checkBoxStyles.switchBase,
										checked: checkBoxStyles.switchChecked,
										thumb: checkBoxStyles.switchIcon,
										track: checkBoxStyles.switchBar,
									}}
								/>
							}
							classes={{
								label: checkBoxStyles.label,
							}}
							label="Set Code/Description length"
						/>
						<IconButton
							size="sm"
							aria-label="info"
							component="span"
							style={{ backgroundColor: "transparent" }}
							onClick={(e) => setAnchorElRight(e.currentTarget)}
						>
							<InfoIcon/>
						</IconButton>
						<Popover
							classes={{
								paper: popOverStyles.popover,
							}}
							open={Boolean(anchorElRight)}
							anchorEl={anchorElRight}
							onClose={() => setAnchorElRight(null)}
							anchorOrigin={{
								vertical: "center",
								horizontal: "right",
							}}
							transformOrigin={{
								vertical: "center",
								horizontal: "left",
							}}
						>
							<h3 className={popOverStyles.popoverHeader}>Default values</h3>
							<div className={popOverStyles.popoverBody}>
								<b>Default Code value (0):</b>
								<br/>
								Calculated by matching the highest code length.
							</div>
							<div className={popOverStyles.popoverBody}>
								<b>Default Description value (0):</b>
								<br/> 140 characters long.
							</div>
						</Popover>
					</Grid>
					{checked ? (
						<CustomValuesComponent
							codeDescValues={codeDescValues}
							handleChange={handleChange}
						/>
					) : null}
				</Grid>
				<Grid item>
					<CustomSnackBar
						status={status}
						open={open}
						handleClose={handleSnackClose}
					/>
				</Grid>
			</Grid>
		</form>
	);
};

export default Main;
